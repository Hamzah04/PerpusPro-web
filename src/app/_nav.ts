import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },

  // Menu Admin
  {
    title: true,
    name: 'Admin'
  },
  {
    name: 'User',
    url: '/theme/colors',
    icon: 'icon-user'
  },
  {
    name: 'Member',
    url: '/theme/typography',
    icon: 'icon-people'
  },
  {
    name: 'Role Group',
    url: '/theme/typography',
    icon: 'icon-people'
  },

  // Menu data Book 

  {
    title: true,
    name: 'Books'
  },
  {
    name: 'All Books',
    url: '/theme/colors',
    icon: 'icon-file'
  },
  {
    name: 'Story Books',
    url: '/theme/typography',
    icon: 'icon-people'
  },
  {
    name: 'History Books',
    url: '/theme/typography',
    icon: 'icon-people'
  },
  {
    name: 'Scientific Books',
    url: '/theme/typography',
    icon: 'icon-people'
  },

  // Menu data Transaction 

  {
    title: true,
    name: 'Transaction'
  },
  {
    name: 'Data Peminjam',
    url: '/theme/colors',
    icon: 'icon-books'
  },
  {
    name: 'Data Pengembalian',
    url: '/theme/typography',
    icon: 'icon-people'
  },
  {
    name: 'Data Jatuh Tempo',
    url: '/theme/typography',
    icon: 'icon-people'
  },

  // Menu Repots
  {
    title: true,
    name: 'Reports'
  },

  {
    name: 'Admin',
    url: '/base',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'User',
        url: '/base/cards',
        icon: 'icon-puzzle'
      },
      {
        name: 'Member',
        url: '/base/carousels',
        icon: 'icon-puzzle'
      },
    ]
  },
  
  {
    name: 'Books',
    url: '/notifications',
    icon: 'icon-bell',
    children: [
      {
        name: 'All Books',
        url: '/notifications/alerts',
        icon: 'icon-bell'
      },
      {
        name: 'Story Books',
        url: '/notifications/badges',
        icon: 'icon-bell'
      },
      {
        name: 'History Books',
        url: '/notifications/modals',
        icon: 'icon-bell'
      },
      {
        name: 'Scientific Books',
        url: '/notifications/modals',
        icon: 'icon-bell'
      }
    ]
  },

  {
    name: 'Transaction',
    url: '/notifications',
    icon: 'icon-bell',
    children: [
      {
        name: 'Data Peminjam',
        url: '/notifications/alerts',
        icon: 'icon-bell'
      },
      {
        name: 'Data Pengembalian',
        url: '/notifications/badges',
        icon: 'icon-bell'
      },
      {
        name: 'Data Jatuh Tempo',
        url: '/notifications/modals',
        icon: 'icon-bell'
      },
    ]
  },
  
  {
    name: 'Pages',
    url: '/pages',
    icon: 'icon-star',
    children: [
      {
        name: 'Login',
        url: '/login',
        icon: 'icon-star'
      },
      {
        name: 'Register',
        url: '/register',
        icon: 'icon-star'
      },
      {
        name: 'Error 404',
        url: '/404',
        icon: 'icon-star'
      },
      {
        name: 'Error 500',
        url: '/500',
        icon: 'icon-star'
      }
    ]
  },
  // {
  //   name: 'Download CoreUI',
  //   url: 'http://coreui.io/angular/',
  //   icon: 'icon-cloud-download',
  //   class: 'mt-auto',
  //   variant: 'success',
  //   attributes: { target: '_blank', rel: 'noopener' }
  // },
  // {
  //   name: 'Try CoreUI PRO',
  //   url: 'http://coreui.io/pro/angular/',
  //   icon: 'icon-layers',
  //   variant: 'danger',
  //   attributes: { target: '_blank', rel: 'noopener' }
  // }
];
